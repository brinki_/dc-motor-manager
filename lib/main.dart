import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';
import 'dart:async';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(title: 'DC Motor Manager'),
    );
  }
}

class HomePage extends StatefulWidget {
  final String title;
  HomePage({Key key, this.title}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState(title);
}

class _HomePageState extends State<HomePage> {
  final String title;
  double duty = 0;
  bool flagMode = true;

Future<String> apiRequest(String url, Map jsonMap) async {
  HttpClient httpClient = new HttpClient();
  HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  request.headers.set('content-type', 'application/json');
  request.add(utf8.encode(json.encode(jsonMap)));
  HttpClientResponse response = await request.close();
  // todo - you should check the response.statusCode
  String reply = await response.transform(utf8.decoder).join();
  httpClient.close();
  return reply;
}

  void setDirection(bool direction) async{

    String sDirection;
    //if 0 --> clockwise
    //if 1 --> counterclockwise
    if(direction)
      sDirection = '0';
    else
      sDirection = '1';

    Map map = {
      'direction' : '$sDirection'
    };

    print(await apiRequest("http://localhost:6000/setDirection", map));

  }

  void setMode(bool mode) async{
    String sMode;
    if(mode)
      sMode = '0';
    else
      sMode = '1';

    Map map = {
      'mode' : '$sMode'
    };

    print(await apiRequest("http://localhost:6000/setMode", map));

  }


  void setSpeed(double speed) async{
    int sSpeed = speed.toInt();

    Map map = {
      'speed' : '$sSpeed'
    };

    print(await apiRequest("http://localhost:6000/setSpeed", map));

  }

  _HomePageState(this.title);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title), 
        leading: Column(
          children: <Widget>[
            Text("Manual Mode", style: TextStyle(fontSize: 8),),
            CupertinoSwitch(
            value: flagMode,
            onChanged: (newValue){
              setMode(newValue);
              setState(() => flagMode = newValue);
            },
          ),
          ]        
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Speed: "),
              Slider(
                value: duty,
                onChanged: (newDuty) {
                  setSpeed(newDuty);
                  setState(() => duty = newDuty);
                },
                min: 0,
                max: 255,
                divisions: 255,
                label: "$duty",
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Direction:"),
              MaterialButton(
                child: Icon(Icons.autorenew, color: Colors.black,),
                onPressed: (){
                  setDirection(false);
                },  

              ),
              MaterialButton(
                child: Icon(Icons.cached, color: Colors.black,),
                onPressed: (){
                  setDirection(true);
                },

              )
            ],
          )

        ],
      ),
    );

  }

}