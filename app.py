from flask import Flask, jsonify, request  #import moduli dal modulo centrale "FLASK"
from flask_cors import CORS

import serial 

isSerialAvailable = False  #Variabile globale utilizzata per vedere se la seriale di Arduino è Disponibile e quindi non occupata

app = Flask("__name__")
CORS(app)

try:                                                              #tentavivo di connessione seriale
    ser = serial.Serial('/dev/tty.usbmodem142401', 115200)        #l'indirizzo è relativo, è da cambiare in base al proprio pc
    isSerialAvailable = True
    print("serial available")
except serial.SerialException:
    print("serial not available")

def printToSerial(string):                                        #funzione per printare le varie stringhe su seriale, convertendole per il protocollo ideato

    ser.write(bytes('<', 'ascii'))                                 #converto i caratteri in byte e li printo su seriale

    for ch in string:
        ser.write(bytes(ch, 'ascii'))

    ser.write(bytes('>', 'ascii'))


@app.route("/setSpeed", methods=["POST"])           #creo un endpoint POST del server accessibile mediante 'setSpeed', 
def setSpeed():                                     #funzione che verrà chiamata quando un utente farà un chiamata all'endpoint
    
    rawSpeed = request.json["speed"]                #speed in formato Strimga, direttamente prelevata dal json body della chiamata dal value con key "speed"
    speed = int(rawSpeed)                           #cast from str to int

    if(speed < 0 or speed >255):                    #controllo validità 
        print("speed not valid")
        return(jsonify({"status" : "error"}))       #se non valido, ritorno al client un json con stato di errore
    else:
        print("speed:", speed)
        if(isSerialAvailable):                      #se è valido e la seriale è disponibile
            string = "S" + rawSpeed                 #aggiungo caratter per identificare l'operazione che Arduino dovrà svolgere
            printToSerial(string)                   #printo su seriale la stringa Finale
        return(jsonify({"status" : "successful"}))  #successivamente ritorno client un json con stato di successo


#la procedura precedentemente spiegata, vale anche per gli altri endpoint con le opportune modifiche relative all'operazione da svolgere

@app.route("/setDirection", methods=["POST"])
def setDirection():
    rawDirection = request.json["direction"]
    direction = int(rawDirection)
    if(direction != 0 and direction != 1):
        print("direction not valid")
        return(jsonify({"status" : "error"}))
    else:
        print("direction:", direction)
        if(isSerialAvailable):
            string = "D" + rawDirection
            printToSerial(string)
        return(jsonify({"status" : "successful"}))



@app.route("/setMode", methods = ["POST"])
def setMode():
    rawMode = request.json["mode"]
    mode = int(rawMode)
    if(mode != 0 and mode != 1):
        print("mode not valid")
        return(jsonify({"status" : "error"}))
    else:
        if(isSerialAvailable):
            string = "M" + rawMode
            printToSerial(string)
        return(jsonify({"status" : "successful"}))

if __name__ == "__main__":
    app.run(debug=True)